chartWrapper = {
	data: null,
	options: null,
	chart: null,
};

// Function to style document
function styleDocument(){
    // Show chart data
    $('#chart-data-container').css('display', 'inline-block');
    // Change letter spacing to 1px
    $('*').filter(function(){
    	return $(this).css('letter-spacing') == '-1px';
    }).css('letter-spacing', '1px');
}

// Sort array of elemWidth
function sortArr(arr){
	arr.sort(function(a, b) {
    	return b.votes - a.votes;
	});
}

// Function to sort elements on DOM
function sortChoices(arr) {
	sortArr(arr);
	parnt = $('#choices-data-container');
	for(var i = 0; i < arr.length; i++){
		parnt.append(arr[i].elem);
	}
}


function elemObj(elem){
	this.elem = elem

	// Find votes
	this.votes = 0;
	try {
		this.votes = parseInt($(this.elem).find('.votes').attr('value'));
	} catch (e){
		console.log(e.stack)
	}

	// Find choice text
	this.choiceText = $(this.elem).find('.choice').attr('value');
}


// Function to populate pie chart
function initChart(arr, sum){

	function trimLabel(label){
		trimLimit = 7
		if (label.length > trimLimit)
			label = label.substring(0, trimLimit) + '...';
		return label
	}

	// Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Choice');
    data.addColumn('number', 'votes');
	for(var i = 0; i < arr.length; i++){
		data.addRow([trimLabel(arr[i].choiceText), arr[i].votes])

	}
	
	var colors = generateColorsList(arr.length);
	
	// Set chart options
    var options = {     legend:'none',
                        width:'100%',
                        height:'100%',
                        backgroundColor:'transparent',
                        titleTextStyle: { color: '#777' },
                        colors: colors,
                        pieSliceText: 'label',
                        chartArea: {
				            height: "94%",
				            width: "94%"
				        }
    };

    var chart = new google.visualization.PieChart($('#chart')[0]);
    chartWrapper.data = data;
    chartWrapper.options = options;
	chartWrapper.chart = chart;
}

// Instantiate and draw our chart, passing in some options.
function drawChart() {
	data = chartWrapper.data;
	options = chartWrapper.options;
	chart = chartWrapper.chart;
    chart.draw(data, options);
}

function loadData(){
	var elems = $('.entry'),
		arr = [],
		votesSum = 0;
	
	for (var i = 0; i < elems.length; i++){
		arr[i] = new elemObj(elems[i]);
		votesSum += arr[i].votes;
	}	

	sortChoices(arr);
    initChart(arr, votesSum);

	// initChart(arr, votesSum);
}

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});
google.setOnLoadCallback(function(){
	styleDocument();
	loadData();
	drawChart();
});

$( window ).on( "throttledresize", drawChart );