// Function to style and prepare document for use
function styleDocument(){
    // Hide no-js class elements
    $('.no-js').css('display', 'none');
    // Change letter spacing to 1px
    $('*').filter(function(){
        return $(this).css('letter-spacing') == '-1px';
    }).css('letter-spacing', '1px');
    $('#vote-form').attr('action', '#').on('submit', checkSubmit);
    $('#popup-ok-button-container').css('display', 'block');
    $('#popup-ok-button').on('click', hidePopup);
}

function showPopup(){
    window.scrollTo(0,0);
    $('#popup-wrapper').css('display', 'inline-block');
    $('#fade').css('display', 'inline-block');  
}


function hidePopup(){
    $('#popup-wrapper').css('display', 'none');
    $('#fade').css('display', 'none');  
}

function alertUser(msg){
    try{
        elem = $('#popup-content').text(msg);
        showPopup();
    } catch (e){
    	console.log(e.stack);
    }
}

function errorOccured(){
    msg = 'Something went wrong, please refresh the page and try again.';
    alertUser(msg, {iserror: true});
}

function checkSubmit() {
    var msg = 'Something went wrong, please refresh the page and try again.';

    try{
        is_any_checked = false;

        $("[name='choices']").each(function(){
            if (this.checked) {
                is_any_checked = true;
                return false;   // break;
            }
        });

        if (!is_any_checked) {
            msg = 'Please select an option.';
            throw new Error('No choices selected.');
        }

        if (recaptcha.enabled && !recaptcha.filled){
            msg = 'Please fill the captcha!';
            throw new Error('Captcha Error');
        }
        
        doAjax();
    } catch(e) {
        console.log(e.stack);
        alertUser(msg);
    } finally {
        return false;
    }
}

function captchaLoaded(){
    recaptcha.load();
}

recaptcha = {
    enabled: false,
    loaded: false,
    created: false,
    cid: 0, 
    filled: false,
    sitekey : '6Le8wRgTAAAAALnsnR2Jw7YIPk6jUrPWx-A68Gr3',
    elem: null,

    load: function() {
        this.loaded = true;
        this.elem = $('.g-recaptcha');   
        if ($('#blocked').length){
            this.enabled = true;
            this.create();
            this.show();
        }
    },

    create: function() {
        if (this.loaded) {
            this.cid = grecaptcha.render(this.elem[0], 
                    { 
                        'sitekey': this.sitekey,
                        'callback': function(){
                            recaptcha.filled = true;
                        } 
            });
            this.created = true;
        }
    },

    show: function(){
        this.elem.show();
    },

    hide: function() {
        this.elem.hide();
    },

    reset: function(){
        if (this.created){
            grecaptcha.reset(this.cid);
            this.filled = false;
        }
    },

    handleResponse: function(blocked) {
        if (blocked){
            this.enabled = true;
            if (!this.loaded)
                errorOccured();
            else if (!this.created)
                this.create();
            else
                this.reset();
            this.show();
        }
        else{
            this.reset();
            this.hide();
            this.enabled = false;
        }
    }
}
function doAjax(){
    $.ajax({
        type: "POST",
        url: "vote/",
        dataType: "json",      
        data : $('#vote-form').serializeArray(),
        success: function (response) {
            try{
                iserror = response['iserror'];
                if (iserror == false) {
                    window.location = $('#results-button').attr('href');
                } else {
                    errorOccured();
                }
            } catch(e) {
                console.log(e.stack);
                errorOccured();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if(xhr.status==403) {
                
            }
        }
    });
}

$(document).ready(styleDocument);