from .models import IpAddress, Question, Vote
from django.db import IntegrityError
from django.utils import timezone
import logging
# Useful functions

logger = logging.getLogger('root_logger')
# Function to return settings


# Function to check if many ips can vote given a question object
def many_ips_can_vote(question):
    if question.restriction == Question.RESTRICTION_ONE_IP:
        return False
    elif question.restriction == Question.RESTRICTION_MANY_IPS:
        return True
    else:
        return False


class IpAddressWrapper(object):
    ip_address_text = None
    ip_address = None

    def block(self):
        if self.ip_address is not None:
            self.ip_address.is_blocked = True
            self.ip_address.save()

    def unblock(self):
        if self.ip_address is not None:
            self.ip_address.is_blocked = False
            self.ip_address.save()

    def is_blocked(self):
        if self.ip_address is None:
            return False
        else:
            return self.ip_address.is_blocked

    def has_voted(self, question):
        if self.ip_address is None:
            return False
        try:
            Vote.objects.get(question=question, ip_address=self.ip_address)
        except Vote.DoesNotExist:
            logger.debug('Ip has not voted for Question:{0}'.format(question))
            return False
        return True

    # Method to register ip address to the specific question
    def register_vote(self, question):
        try:
            if self.ip_address is None:
                self.ip_address, _ = IpAddress.objects.get_or_create(
                                        ip_address_text=self.ip_address_text)
            Vote.objects.get_or_create(question=question,
                                       ip_address=self.ip_address,
                                       vote_date=timezone.now())
        except IntegrityError as e:
            logger.exception(e)
            return False
        return True

    def __init__(self, request):
        self.ip_address_text = self.__get_client_ip(request)
        self.ip_address = self.__get_ip_address(request)

    # Method to get the ip address from database.
    def __get_ip_address(self, request):
        try:
            return IpAddress.objects.get(ip_address_text=self.ip_address_text)
        except IpAddress.DoesNotExist:
            return None

    # Method to get the clients ip based on the request
    def __get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
