from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.core.urlresolvers import reverse
from django.utils import timezone
# from ratelimit.decorators import ratelimit
from ratelimit.utils import is_ratelimited
from .models import Question, Choice
from .forms import PollForm, VoteForm, CaptchaForm
from .utils import many_ips_can_vote, IpAddressWrapper
import logging

# Get an instance of a logger
logger = logging.getLogger('root_logger')


# Create your views here.

# Basic class view
class BasicView(View):
    template_name = None


class RateLimitedView(BasicView):
    key = 'ip'
    rate = '1/m'
    group = 'default'
    increment = True

    def check_block(self, request, *args, **kwargs):
        return is_ratelimited(request, key=self.key,
                              group=self.group, rate=self.rate,
                              increment=self.increment)


# Class that has the same behavior against post and get requests
class GetPostView(BasicView):

    # Implementation in subclasses
    def _get_post(self, request, *args, **kwargs):
        pass

    def get(self, request, *args, **kwargs):
        return self._get_post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self._get_post(request, *args, **kwargs)


class CreatePollView(RateLimitedView):
    rate = '5/m'

    def get(self, request):
        return redirect(reverse('polls:index'))

    # Checks if POST request is valid and register vote
    def create_poll(self, cleaned_data):
        question = Question.objects.create(
                        question_text=cleaned_data.get('question'),
                        pub_date=timezone.now(),
                        question_type=cleaned_data.get('question_type'),
                        restriction=cleaned_data.get('question_restriction')
                    )
        # Also create the choices
        for choice in cleaned_data.get('choices'):
            Choice.objects.create(question=question, choice_text=choice)

        return question.id

    def handle_ajax(self, request):
        # Check if ip is blocked

        # Check if ip is blocked
        ip = IpAddressWrapper(request)
        if ip.is_blocked():
            form = CaptchaForm(request.POST)
            if form.is_valid():
                ip.unblock()
            else:
                return JsonResponse({'iserror': True})

        blocked = self.check_block(request)
        if blocked:
            ip.block()

        form = PollForm(request.POST)
        if form.is_valid():
            qid = self.create_poll(form.cleaned_data)
            return JsonResponse({'iserror': False, 'next_url': qid,
                                 'blocked': blocked})
        else:
            return JsonResponse({'iserror': True, 'blocked': blocked})

    def handle_normal(self, request):
        if self.check_block(request):
            ratelimit_error = ErrorView.ratelimit_error
            url = reverse('polls:index')
            return ErrorView.redirect_to_error(url, ratelimit_error)

        form = PollForm(request.POST)
        if form.is_valid():
            qid = self.create_poll(form.cleaned_data)
            return redirect(reverse('polls:poll', args=(qid,)))
        else:
            return ErrorView.redirect_to_error(reverse('polls:index'))

    def post(self, request):
        if request.is_ajax():
            return self.handle_ajax(request)
        else:
            return self.handle_normal(request)


class VoteView(RateLimitedView):
    rate = '10/m'

    def get(self, request, question_id):
        return redirect(reverse('polls:poll',
                                args=(question_id,)))

    # Register the vote, meaning that the client's ip has
    # voted for this question, and add votes
    def register_votes(self, request, form):
        # If not multi vote try to register ip
        # If that fails return False
        check = True
        multi_vote = many_ips_can_vote(form.question)
        if not multi_vote:
            check = IpAddressWrapper(request).register_vote(form.question)
        if not check:
            return False
        # Add votes to choice
        for choice in form.cleaned_data['choices']:
            choice.votes += 1
            choice.save()

        return True

    def check_meta(self, request, question):
        multi_vote = many_ips_can_vote(question)
        voted = IpAddressWrapper(request).has_voted(question)
        check = multi_vote or not voted
        return check

    def handle_ajax(self, request, question_id):
        # Check if ip is blocked
        ip = IpAddressWrapper(request)
        if ip.is_blocked():
            form = CaptchaForm(request.POST)
            if form.is_valid():
                ip.unblock()
            else:
                return JsonResponse({'iserror': True})

        if self.check_block(request):
            ip.block()

        question = get_object_or_404(Question, pk=question_id)
        check = self.check_meta(request, question)
        form = VoteForm(request.POST, question=question)
        check = check and form.is_valid()

        check = check and self.register_votes(request, form)
        if check:
            return JsonResponse({'iserror': False})
        else:
            return JsonResponse({'iserror': True})

    def handle_normal(self, request, question_id):
        if self.check_block(request):
            ratelimit_error = ErrorView.ratelimit_error
            url = reverse('polls:poll', args=(question_id,))
            return ErrorView.redirect_to_error(url, ratelimit_error)

        question = get_object_or_404(Question, pk=question_id)

        check = self.check_meta(request, question)
        form = VoteForm(request.POST, question=question)
        check = check and form.is_valid()
        check = check and self.register_votes(request, form)
        if check:
            return redirect(reverse('polls:results', args=(question.id,)))
        else:
            return redirect(reverse('polls:poll', args=(question.id,)))

    def post(self, request, question_id):
        if request.is_ajax():
            return self.handle_ajax(request, question_id)
        else:
            return self.handle_normal(request, question_id)


# Class to display index page
@method_decorator(never_cache, name='get')
@method_decorator(never_cache, name='post')
class IndexView(GetPostView):
    template = 'polls/index.html'

    def _get_post(self, request):
        form = PollForm()
        blocked = IpAddressWrapper(request).is_blocked()
        return render(request, self.template,
                      {'form': form, 'blocked': blocked})


# Class to display poll
@method_decorator(never_cache, name='get')
@method_decorator(never_cache, name='post')
class PollView(GetPostView):
    template = 'polls/poll.html'

    def _get_post(self, request, question_id=None):
        # If question does not exist return 404
        question = get_object_or_404(Question, pk=question_id)
        # If many ips can vote or the user has not voted for this poll
        # set can_vote to True else False
        can_vote = (many_ips_can_vote(question) or
                    not IpAddressWrapper(request).has_voted(question))
        form = VoteForm(question=question)
        blocked = IpAddressWrapper(request).is_blocked()
        return render(request, self.template, {
                       'question': question, 'can_vote': can_vote,
                       'type': Question.TYPE_CHOICES, 'form': form,
                       'blocked': blocked
                    })


# Class to display results
class ResultsView(GetPostView):
    template = 'polls/results.html'

    def _get_post(self, request, question_id=None):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, self.template, {'question': question})


# Class to display an error
class ErrorView(GetPostView):
    template = 'polls/error.html'
    default_error = 0
    ratelimit_error = 1

    def construct_error_url(url, error=None):
        if error is None:
            error = ErrorView.default_error
        final_url = reverse('polls:error')
        final_url += "?%s%s" % ('next=', url)
        final_url += "&%s%s" % ('error=', error)
        return final_url

    def redirect_to_error(url, error=None):
        final_url = ErrorView.construct_error_url(url, error)
        return HttpResponseRedirect(final_url)

    def _get_post(self, request):
        nxt = request.GET.get('next', default=reverse('polls:index'))
        error = request.GET.get('error', default=self.default_error)
        return render(request, self.template, {'next': nxt, 'error': error})
