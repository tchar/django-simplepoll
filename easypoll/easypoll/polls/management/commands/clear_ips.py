from django.core.management.base import BaseCommand, CommandError
from polls.models import Question, Vote, IpAddress
from django.utils import timezone


class Command(BaseCommand):
    help = 'Delete expired ip addresses'

    def delete_expired_votes(self):
        now = timezone.now()
        ip_addresses = set([])
        for vote in Vote.objects.all():
            td = now-vote.vote_date
            if td.days > 0:
                vote.delete()
                self.stdout.write(self.style.SUCCESS(
                                    'Deleted vote: {0}'.format(vote)))
            else:
                ip_addresses.add(vote.ip_address)

        # Clear all ip addresses that are not associated with any votes
        ip_addresses_all = set(IpAddress.objects.all())
        for ip in ip_addresses_all-ip_addresses:
            ip.delete()
            self.stdout.write(self.style.SUCCESS(
                                    'Deleted ip: {0}'.format(vote)))

    def handle(self, *args, **options):
        self.delete_expired_votes()
