from django.db import models

# Create your models here.


class Question(models.Model):
    TYPE_SINGLE = 0
    TYPE_MULTI = 1
    TYPE_CHOICES = (
        (TYPE_SINGLE, 'Single choice'),
        (TYPE_MULTI, 'Multiple choice'),
    )
    # To use for POST data validation
    TYPES_LIST = [v[0] for v in TYPE_CHOICES]

    RESTRICTION_ONE_IP = 0
    RESTRICTION_MANY_IPS = 1
    RESTRICTIONS = (
        (RESTRICTION_ONE_IP, 'Users can vote only once'),
        (RESTRICTION_MANY_IPS, 'Users can vote many times')
    )
    # To use for POST data validation
    RESTRICTIONS_LIST = [v[0] for v in RESTRICTIONS]

    question_text = models.CharField(max_length=200, null=False, blank=False)
    pub_date = models.DateTimeField('date published', null=False, blank=False)
    question_type = models.IntegerField(default=TYPE_SINGLE,
                                        choices=TYPE_CHOICES)
    restriction = models.IntegerField(default=RESTRICTION_ONE_IP,
                                      choices=RESTRICTIONS)
    total_votes = models.IntegerField(default=0)

    def __str__(self):
        return ('id:{0} - {1}'.format(self.id, self.question_text))


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE,
                                 null=False, blank=False)
    choice_text = models.CharField(max_length=200, null=False, blank=False)
    votes = models.IntegerField(default=0)

    __original_votes = None

    def __init__(self, *args, **kwargs):
        super(Choice, self).__init__(*args, **kwargs)
        self.__original_votes = self.votes

    def percentage(self):
        if self.question.total_votes == 0:
            return 0
        return round(1000 * self.votes / self.question.total_votes) / 10

    def __str__(self):
        return ('Q:{0} - V:{1} - {2}'.format(self.question.question_text,
                                             self.votes, self.choice_text))

    def save(self, *args, **kwargs):
        # If votes changed
        if (self.votes != self.__original_votes):
            self.question.total_votes += self.votes - self.__original_votes
            self.question.save()
        super(Choice, self).save(*args, **kwargs)
        self.__original_votes = self.votes


class IpAddress(models.Model):
    ip_address_text = models.GenericIPAddressField(
                                protocol='both', unpack_ipv4=True, unique=True,
                                null=False, blank=False)

    is_blocked = models.BooleanField(default=False)

    def __str__(self):
        return ('{0} - is blocked:{1}'
                .format(self.ip_address_text, self.is_blocked))


class Vote(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE,
                                 null=False, blank=False)
    ip_address = models.ForeignKey(IpAddress, on_delete=models.CASCADE,
                                   null=False, blank=False)
    vote_date = models.DateTimeField('date voted', null=False, blank=False)

    class Meta:
        unique_together = ('question', 'ip_address',)

    def __str__(self):
        return str('{0} - {1}'.format(self.question, self.ip_address))
