from django import forms
from django.conf import settings
from .models import Question
from captcha.fields import ReCaptchaField
import logging


# Get an instance of a logger
logger = logging.getLogger('root_logger')


# This is a CharField for ContactForm
class PollFormCharField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super(PollFormCharField, self).__init__(
                    max_length=settings.MAX_CHOICE_LENGTH,
                    *args, **kwargs)


class NoIdForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(NoIdForm, self).__init__(*args, **dict(kwargs, auto_id=False))


class CaptchaForm(forms.Form):
    captcha = ReCaptchaField()


class PollForm(NoIdForm):

    # Form fields
    question = PollFormCharField()
    choices = PollFormCharField(required=False)
    question_type = forms.ChoiceField(choices=Question.TYPE_CHOICES)
    question_restriction = forms.ChoiceField(choices=Question.RESTRICTIONS)

    def clean(self):
        super(PollForm, self).clean()

        choices = []
        for choice in self.data.getlist('choices'):
            if not choice or choice.isspace():
                continue
            choices.append(choice[:200].strip())
        self.cleaned_data['choices'] = choices

    def is_valid(self):
        is_valid = super(PollForm, self).is_valid()
        choices = self.cleaned_data['choices']
        is_valid = is_valid and len(choices) >= 2
        is_valid = is_valid and len(choices) <= settings.MAX_CHOICES
        return is_valid


class VoteForm(NoIdForm):
    choices = forms.ChoiceField()
    question = None

    def __init__(self, *args, **kwargs):
        try:
            self.question = kwargs.pop('question')
        except KeyError:
            pass
        super(VoteForm, self).__init__(*args, **kwargs)

        if self.question:
            choices = []
            for choice in self.question.choice_set.all():
                choices.append((choice.id, choice.choice_text))
            self.fields['choices'] = forms.ChoiceField(choices=choices)

    def clean(self):
        super(VoteForm, self).clean()

        if not self.question:
            return

        choices = []
        for choice_id in self.data.getlist('choices'):
            try:
                choice_id = int(choice_id)
                choice = self.question.choice_set.get(pk=choice_id)
                choices.append(choice)
            except Exception as e:
                logger.exception(e)
        self.cleaned_data['choices'] = choices

    def is_valid(self):
        if not self.question:
            return False

        if len(self.data.getlist('choices')) > settings.MAX_CHOICES:
            return False

        is_valid = super(VoteForm, self).is_valid()

        choices = self.cleaned_data['choices']

        single = self.question.question_type == Question.TYPE_SINGLE
        # If single
        if single and len(choices) == 1:
                return True and is_valid
        # Else if multi
        elif len(choices) > 0:
                return True and is_valid
        return False

# class ChoicesForm(forms.Form):
#     choices = PollFormCharField()

#     def __init__(self, *args, **kwargs):
#         super(ChoicesForm, self).__init__(*args, **dict(kwargs, auto_id=False))

# ChoicesFormset = forms.formset_factory(ChoicesForm, min_num=1,
#                                        validate_min=True, extra=3)
