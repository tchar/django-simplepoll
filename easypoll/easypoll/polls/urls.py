from django.conf.urls import url

from . import views

app_name = 'polls'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^createpoll/$', views.CreatePollView.as_view(), name='createpoll'),
    url(r'^error/$', views.ErrorView.as_view(), name='error'),
    url(r'^(?P<question_id>[0-9]+)/$', views.PollView.as_view(), name='poll'),
    url(r'^(?P<question_id>[0-9]+)/results/$',
        views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$',
        views.VoteView.as_view(), name='vote'),
]
