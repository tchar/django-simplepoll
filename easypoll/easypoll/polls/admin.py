from django.contrib import admin

# Register your models here.

from .models import Question, Choice, Vote, IpAddress

admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Vote)
admin.site.register(IpAddress)
